﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public class Day02 : Solution {
		public Day02() : base(2) { }

		public override string Part1(string input) {
			var intcode = input.Split(',').Select(s => Int32.Parse(s)).ToArray();
			intcode[1] = 12;
			intcode[2] = 2;

			Utility.RunIntcodeProgram(intcode);
			return intcode[0].ToString();
		}

		public override string Part2(string input) {
			var intcode_input = input.Split(',').Select(s => Int32.Parse(s)).ToArray();
			int[] intcode;
			for (int noun = 0; noun < 100; noun++) {
				for (int verb = 0; verb < 100; verb++) {
					intcode = (int[])intcode_input.Clone();
					intcode[1] = noun;
					intcode[2] = verb;

					Utility.RunIntcodeProgram(intcode);
					if (intcode[0] == 19690720)
						return (100 * noun + verb).ToString();
				}
			}
			return null;
		}
	}
}
