﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public class Day05 : Solution {
		public Day05() : base(5) { }

		public override string Part1(string input) {
			var intcode = input.Split(',').Select(s => Int32.Parse(s)).ToArray();

			using var reader = new StringReader("1");
			return Utility.RunIntcodeProgram(intcode, reader).ToString();
		}

		public override string Part2(string input) {
			var intcode = input.Split(',').Select(s => Int32.Parse(s)).ToArray();

			using var reader = new StringReader("5");
			return Utility.RunIntcodeProgram(intcode, reader).ToString();
		}
	}
}
