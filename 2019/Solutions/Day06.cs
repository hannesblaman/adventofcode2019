﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public class Day06 : Solution {
		public Day06() : base(6) { }

		public override string Part1(string input) {
			var orbits = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			foreach (var line in ReadLines(input)) {
				var match = Regex.Match(line, @"(?<host>.+)\)(?<resident>.+)");
				orbits[match.Groups["resident"].Value] = match.Groups["host"].Value;
			}

			uint checksum = 0;
			foreach (var key in orbits.Keys) {
				checksum += GetChecksum(key, checksum);
			}

			return checksum.ToString();

			uint GetChecksum(string key, uint cur) {
				var value = orbits[key];
				if (!value.Equals("com", StringComparison.OrdinalIgnoreCase)) {
					return GetChecksum(value, cur) + 1;
				}
				return 1;
			}
		}

		public override string Part2(string input) {
			var orbits = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			foreach (var line in ReadLines(input)) {
				var match = Regex.Match(line, @"(?<host>.+)\)(?<resident>.+)");
				orbits[match.Groups["resident"].Value] = match.Groups["host"].Value;
			}

			var you = StepsTilCOM("YOU");
			var san = StepsTilCOM("SAN");

			var firstCommon = you.Intersect(san).First();

			return (you.IndexOf(firstCommon) + san.IndexOf(firstCommon)).ToString();

			List<string> StepsTilCOM(string key, List<string> cur = null) {
				cur ??= new List<string>();
				var value = orbits[key];
				if (!value.Equals("com", StringComparison.OrdinalIgnoreCase)) {
					cur.Add(value);
					StepsTilCOM(value, cur);
				}
				return cur;
			}
		}
	}
}
