﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public class Day07 : Solution {
		public Day07() : base(7) { }

		public override string Part1(string input) {
			var intcode = input.Split(',').Select(s => Int32.Parse(s)).ToArray();

			var max = 0;
			foreach (int[] permutation in Utility.GetPermutations(Enumerable.Range(0, 5).ToArray())) {
				Console.WriteLine(String.Join("", permutation));
				int lastOutput = 0;
				foreach (var phase in permutation) {
					using var reader = new StringReader($"{phase}\n{lastOutput}");
					lastOutput = Utility.RunIntcodeProgram((int[])intcode.Clone(), reader);
				}
				if (lastOutput > max)
					max = lastOutput;
			}
			return max.ToString();
		}

		public override string Part2(string input) {
			//TODO
			var intcode = input.Split(',').Select(s => Int32.Parse(s)).ToArray();

			var max = 0;
			foreach (int[] permutation in Utility.GetPermutations(Enumerable.Range(5, 5).ToArray())) {
				Console.WriteLine(String.Join("", permutation));
				int lastOutput = 0;
				foreach (var phase in permutation) {
					using var reader = new StringReader($"{phase}\n{lastOutput}");
					lastOutput = Utility.RunIntcodeProgram((int[])intcode.Clone(), reader);
				}
				if (lastOutput > max)
					max = lastOutput;
			}
			return max.ToString();
		}
	}
}
