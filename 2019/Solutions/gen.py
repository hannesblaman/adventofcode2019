import re
day1 = open("Day1.cs", "r").read()
for i in range(2, 26):
	with open(f"Day{i}.cs", "w+") as dayi:
		dayi.write(re.sub(r'(?<!part)1', str(i), day1, flags=re.IGNORECASE))